<?php

/**
 * Initialize theme settings
 *
 * Used when the user install the theme for the fist time
 * or when there is a new option in the theme settings
 */
function plainscape_init_settings() {
  global $theme_key;

  $defaults = plainscape_get_default_settings();
  $settings = theme_get_settings($theme_key);
  plainscape_reset_content_type($settings);

  // Don't save the toggle_node_info_ variables
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_'. $type]);
    }
  }

  // Save default theme settings
  variable_set(
    str_replace('/', '_', 'theme_' . $theme_key . '_settings'),
    array_merge($defaults, $settings)
  );

  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

/**
 * Returns default theme settings
 *
 * @return
 *    array Default theme settings array
 */
function plainscape_get_default_settings() {
  $defaults = array(
    'enable_content_type' => 0,
    'wp_readmore_default' => 1,
    'wp_readmore_weight' => 20,
    'prev_next_links_default' => 1,
    'breadcrumb_display' => 1,
    'subscribe_link_display' => 1,
    'recent_comments' => 5,
  );

  return array_merge($defaults, theme_get_settings());
}

/**
 * Returns current theme settings
 *
 * @return
 *    array Current theme settings array
 */
function plainscape_get_settings($saved_settings) {
  $defaults = plainscape_get_default_settings();

  // Set the default values for content-type-specific settings
  foreach (node_get_types('names') as $type => $name) {
    $defaults["wp_readmore_{$type}"]     = $defaults['wp_readmore_default'];
    $defaults["prev_next_links_{$type}"] = $defaults['prev_next_links_default'];
  }

  $settings = array_merge($defaults, $saved_settings);
  plainscape_reset_content_type($settings);

  return $settings;
}

function plainscape_reset_content_type(&$settings) {
  // If content type-specifc settings are not enabled, reset the values
  if ($settings['enable_content_type'] == 0) {
    foreach (node_get_types('names') as $type => $name) {
      $settings["wp_readmore_{$type}"]     = $settings['wp_readmore_default'];
      $settings["prev_next_links_{$type}"] = $settings['prev_next_links_default'];
    }
  }
}