<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <!--[if lt IE 7]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . drupal_get_path('theme', 'plainscape') ?>/css/ie.css" />
    <![endif]-->
    <?php print $scripts ?>
  </head>
  <body>
    <div id="wrapper" class="container_12">
      <div id="header" class="grid_12">
        <div id="header-left" class="grid_8 alpha">
          <?php if ($logo): ?>
            <a href="<?php print check_url($front_page) ?>" title="<?php print t('Go to Homepage') ?>"><?php print '<img src="'. check_url($logo) .'" alt="'. $site_name .'" id="logo" />' ?></a>
          <?php endif; ?>
          <?php if ($site_name): ?>
            <h1 id="site-name"><a href="<?php print check_url($front_page) ?>" title="Go to Homepage"><?php print $site_name ?></a></h1>
          <?php endif; ?>
          <?php if ($site_slogan): ?>
            <div id="site-slogan"><?php print $site_slogan ?></div>
          <?php endif; ?>
        </div><!-- #header-left -->
        <div id="header-right" class="grid_4 omega">
          <?php if ($search_box): ?>
            <div id="search-box"><?php print $search_box ?></div>
          <?php endif; ?>
        </div><!-- #header-right -->
        <div class="clear"></div>
      </div><!-- #header -->
      <div class="clear"></div>
      <div id="primary-nav" class="grid_12">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links) ?>
        <?php endif; ?>
      </div><!-- #primary-nav -->
      <div class="clear"></div>
      <div id="content-wrapper" class="grid_12">
        <div id="main" class="<?php print $main_classes ?>">
          <?php print $breadcrumb ?>
          <?php if ($content_top) : ?>
            <div id="content-top">
              <?php print $content_top ?>
            </div>
          <?php endif; ?>
          <?php if ($tabs) : ?>
            <ul class="tabs"><?php print $tabs ?></ul>
          <?php endif; ?>
          <?php if ($title && !$node): ?>
            <h2><?php print $title ?></h2>
          <?php endif; ?>
          <?php if ($show_messages && $messages) : ?>
            <?php print $messages ?>
          <?php endif; ?>
          <?php if ($help) : ?>
            <?php print $help ?>
          <?php endif; ?>
          <?php print $content ?>
          <?php if ($content_bottom) : ?>
            <div id="content-bottom">
              <?php print $content_bottom ?>
            </div>
          <?php endif; ?>
        </div>
        <?php if ($right): ?>
          <div id="sidebar" class="grid_4 omega">
            <?php print $right ?>
          </div>
        <?php endif; ?>
        <div class="clear"></div>
      </div><!-- #content-wrapper -->
      <div class="clear"></div>
      <div id="footer-top" class="grid_12">
        <?php print $footer_top ?>
      </div>
      <div id="footer" class="grid_12">
        <?php if ($footer_message): ?>
          <p><?php print $footer_message ?></p>
        <?php endif; ?>
        <?php print $footer ?>
      </div><!-- #footer -->
      <div class="clear"></div>
    </div><!-- #wrapper -->
    <?php print $closure ?>
  </body>
</html>
