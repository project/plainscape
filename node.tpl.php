<?php
?>
<?php if ($node_top && !$teaser) : ?>
  <div id="node-top">
    <?php print $node_top ?>
  </div>
<?php endif; ?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  <?php if ($page == 0): ?>
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php else: ?>
    <h2 class="title"><?php print $title ?></h2>
  <?php endif; ?>
  <div class="meta">
    <?php if ($submitted): ?>
      <span class="submitted"><?php print $submitted ?></span>
    <?php endif; ?>
  </div>
  <div class="content">
    <?php print $content ?>
  </div>
  <div class="meta node-links">
    <?php if ($terms): ?>
      <div class="terms terms-inline"><?php print $terms ?></div>
    <?php endif;?>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>
  <?php if (!$teaser && $prev_next_links) : ?>
    <div class="previous-next-links">
      <?php print $prev_next_links ?>
    </div>
  <?php endif; ?>
</div>
<?php if ($node_bottom && !$teaser) : ?>
  <div id="node-bottom">
    <?php print $node_bottom ?>
  </div>
<?php endif; ?>
