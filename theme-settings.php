<?php

require_once('theme-settings.inc');

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function phptemplate_settings($saved_settings) {
  // Get theme settings
  $settings = plainscape_get_settings($saved_settings);

  /**
   * Create theme settings form widgets using Forms API
   */
  
  // Plainscape fieldset
  $form['plainscape_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plainscape settings'),
    '#description' => t('Use these settings to change what and how information is displayed in your theme.'),
    '#collapsible' => TRUE,
    '#collapsed' => false,
  );
  
  // General Settings
  $form['plainscape_container']['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['plainscape_container']['general_settings']['breadcrumb_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display breadcrumb'),
    '#default_value' => $settings['breadcrumb_display'],
  );

  $form['plainscape_container']['general_settings']['subscribe_link_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display "Subscribe to feed" link'),
    '#default_value' => $settings['subscribe_link_display'],
  );

  $form['plainscape_container']['general_settings']['recent_comments'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Number of comments in Recent Comments block'),
      '#description'   => t('Number of comments to show in Recent Comments block. Default is 5.'),
      '#default_value' => $settings["recent_comments"],
  );

  // Node Settings
  $form['plainscape_container']['node_type_specific'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node settings'),
    '#description' => t('Here you can make adjustments to which feature to use with your content. You can modify these settings so they apply to all content types, or check the "Use content-type specific settings" box to customize them for each content type. For example, you may want to show "next" and "previous" links on stories, but not pages.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  // Default & content-type specific settings
  foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
    $form['plainscape_container']['node_type_specific']['link_settings'][$type] = array(
      '#type' => 'fieldset',
      '#title' => t('!name', array('!name' => $name)),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['plainscape_container']['node_type_specific']['link_settings'][$type]["wp_readmore_{$type}"] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Enable WordPress-style "Read more" link'),
      '#default_value' => $settings["wp_readmore_{$type}"],
    );
    $form['plainscape_container']['node_type_specific']['link_settings'][$type]["prev_next_links_{$type}"] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Enable "Previous" and "Next" links'),
      '#default_value' => $settings["prev_next_links_{$type}"],
    );
    // Options for default settings
    if ($type == 'default') {
      $form['plainscape_container']['node_type_specific']['link_settings']['default']['#title'] = t('Default');
      $form['plainscape_container']['node_type_specific']['link_settings']['default']['#collapsed'] = $settings['enable_content_type'] ? TRUE : FALSE;
      $form['plainscape_container']['node_type_specific']['link_settings']['wp_readmore_weight'] = array(
        '#type'          => 'textfield',
        '#title'         => t('"Read more" weight'),
        '#description'   => t('You can set the weight of the WordPress-style "Read more" link in case it conflicts with other modules and show up in the wrong order.'),
        '#default_value' => $settings["wp_readmore_weight"],
    );
      $form['plainscape_container']['node_type_specific']['link_settings']['enable_content_type'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Use custom settings for each content type instead of the default above'),
        '#default_value' => $settings['enable_content_type'],
      );
    }
    // Collapse content-type specific settings if default settings are being used
    else if ($settings['enable_content_type'] == 0) {
      $form['enable_links'][$type]['#collapsed'] = TRUE;
    }
  }
  
  return $form;
}