<?php
?>
<div id="comments">
  <?php if (isset($comments_count)): ?>
    <h2><?php print $comments_count ?></h2>
  <?php endif; ?>
  <div class="comment-wrapper">
    <?php print $content; ?>
  </div>
</div>
