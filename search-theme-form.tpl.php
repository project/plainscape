<?php
?>
<label for="quick-search"><?php print t('Search') ?>:</label>
<input class="search-box" id="quick-search" type="text" name="search_theme_form" value="" title="Enter the terms you wish to search for." />
<?php print $search['hidden'] ?>
<input class="search-button" alt="Search" type="image" name="op" value="Search" title="Search" src="<?php print base_path() . path_to_theme() ?>/images/search.png" />