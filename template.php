<?php

require_once('theme-settings.inc');

/**
 * Initialize theme settings
 */
if (is_null(theme_get_setting('wp_readmore_weight'))) {
  plainscape_init_settings();
}

function phptemplate_preprocess_comment_wrapper(&$vars) {
  // get total number of comments
  $comments_count = comment_num_all($vars['node']->nid);

  if ($comments_count == 0) {
    $vars['comments_count'] = t('No comments yet');
  }
  else {
    $vars['comments_count'] = $comments_count . format_plural($comments_count, ' comment', ' comments');
  }
}

function phptemplate_preprocess_comment(&$vars) {
  // format $submitted
  $vars['submitted'] = l($vars['date'],
    'node/' . $vars['node']->nid,
    array('fragment' => 'comment-' . $vars['comment']->cid)
  );

  // change (not verified) to (visitor)
  $vars['author'] = str_replace(t('not verified'), t('visitor'), $vars['author']);
  
  // add a special class for node author
  if ($vars['comment']->uid == $vars['node']->uid) {
    $vars['author_comment'] = true;
  }
}

function phptemplate_preprocess_page(&$vars) {
  if (theme_get_setting('subscribe_link_display') == 1) {
    $url = url('rss.xml', array('absolute' => TRUE));

    // use feedburner url if the module exists
    if (module_exists('feedburner')) {
      $feedburner = db_result(db_query("SELECT feedburner FROM {feedburner} WHERE path = '%s'", 'rss.xml'));
      $url = _feedburner_construct_url($feedburner);
    }

    // add "Subscribe to feed" menu to $primary_links
    $vars['primary_links']['menu-rss'] = array(
      'href' => $url,
      'title' => t('Subscribe to feed'),
      'attributes' => array('title' => t('Subscribe to RSS feed')),
    );
  }
  
  if (theme_get_setting('breadcrumb_display') == 0) {
    $vars['breadcrumb'] = '';
  }
  
  // set classes for "main" div
  // if no right sidebar content, expand the main div to full width
  $vars['main_classes'] = empty($vars['right']) ? 'grid_12' : 'grid_8 alpha';

  // prepare variables for footer credits
  $credits['drupal'] = l('Drupal', 'http://drupal.org', array('attributes' => array('title' => 'Official website of Drupal')));
  $credits['plainscape'] = l('Plainscape', 'http://drupal.org/project/plainscape', array('attributes' => array('title' => 'Plainscape theme project page')));
  $credits['original-author'] = l('Srini', 'http://srinig.com/wordpress/themes/plainscape', array('attributes' => array('title' => 'Original WordPress theme author')));
  $credits['author'] = l('kong', 'http://suksit.com', array('attributes' => array('title' => 'Drupal theme author')));
  
  $vars['footer'] .= t('!plainscape original design by !original-author. Ported to Drupal by !author.', array('!plainscape' => $credits['plainscape'], '!original-author' => $credits['original-author'], '!author' => $credits['author']));
}

function phptemplate_preprocess_node(&$vars) {
  // Add node regions content
  $vars['node_top'] = theme('blocks', 'node_top');
  $vars['node_bottom'] = theme('blocks', 'node_bottom');
  
  // format $submitted
  if (isset($vars['submitted']) && $vars['submitted'] != '') {
    $author = theme('username', $vars['node']);
    $vars['submitted'] = t('Posted on @date, by !author',
      array(
        '@date' => format_date($vars['created'], 'medium'),
        '!author' => $author,
      )
    );
  }

  $wp_readmore_content_type = (theme_get_setting('enable_content_type') == 1) ? $vars['node']->type : 'default';
  $wp_readmore_setting = (theme_get_setting('wp_readmore_'. $wp_readmore_content_type) == 1);

  // format node links
  if (!empty($vars['node']->links)) {
    // add "Read the rest of this entry" link to the end of teaser like in WordPress
    if ($wp_readmore_setting && isset($vars['node']->links['node_read_more'])) {
      $read_more_link = l(t('Read the rest of this entry') . ' »',
        'node/'. $vars['node']->nid,
        array(
          'attributes' => array(
            'title' => t('Read the rest of "@node-title"', array('@node-title' => $vars['node']->title))
          ),
          'query' => NULL,
          'fragment' => NULL,
          'absolute' => FALSE,
          'html' => TRUE,
        )
      );

      $content = $vars['node']->content;

      $content['read-more'] = array(
        '#weight' => (int)theme_get_setting('wp_readmore_weight'),
        '#value' => '<p class="read-more">' . $read_more_link . '</p>',
        '#title' => '',
        '#description' => '',
      );
      
      // experimental code to handle CCK fields
      foreach ($content as $key => $value) {
        if (substr($key, 0, 6) == 'field_' && strpos($vars['content'], $vars[$key . '_rendered']) !== FALSE) {
          $content['cck_' . $key] = array(
            '#weight' => $content[$key]['#weight'],
            '#value' => $vars[$key . '_rendered'],
            '#title' => '',
            '#description' => '',
          );
        }
      }
      
      plainscape_prepare_content($content);

      $vars['content'] = drupal_render($content);

      // remove the original "read more" link from $links
      unset($vars['node']->links['node_read_more']);
      
      // refresh $links
      $vars['links'] = theme('links', $vars['node']->links, array('class' => 'links inline'));
    }
  }

  // format taxonomy list, code adapted from "Acquia Marina" theme
  if (module_exists('taxonomy')) {
    $vocabularies = taxonomy_get_vocabularies($vars['node']->type);
    $output = '';
    $vocab_delimiter = '';
    foreach ($vocabularies as $vocabulary) {
      $terms = taxonomy_node_get_terms_by_vocabulary($vars['node'], $vocabulary->vid);
      if ($terms) {
        $output .= '<li class="vocab vocab-'. $vocabulary->vid .'"><span class="vocab-name">'. $vocabulary->name .':</span> <span class="vocab-list">';
        $links = array();
        foreach ($terms as $term) {        
          $links[] = l($term->name, taxonomy_term_path($term), array('attributes' => array('rel' => 'tag', 'title' => strip_tags($term->description))));
        }

        //$output .= $vocab_delimiter;    // Add comma between vocabularies
        //$vocab_delimiter = ', ';        // Use a comma delimiter after first displayed vocabulary

        $output .= implode(", ", $links);
        $output .= '</span></li>';
      }
    }
    if ($output != '') {
      $output = '<ul class="taxonomy">'. $output .'</ul>';
    }
    $vars['terms'] = $output;
  }
  else {
    $vars['terms'] = '';
  }

  // format node links
  if (isset($vars['node']->links['comment_add'])) {
    if ($vars['teaser']) {
      $vars['node']->links['comment_add']['title'] = t('No comments') . ' »';
    }
  }

  if (isset($vars['node']->links['comment_new_comments'])) {
    $new_comments_title = $vars['node']->links['comment_new_comments']['title'];
    $vars['node']->links['comment_new_comments']['title'] = '(' . $new_comments_title . ')';
  }

  if (isset($vars['node']->links['comment_comments'])) {
    $vars['node']->links['comment_comments']['title'] .= ' »';
  }

  $vars['links'] = theme('links', $vars['node']->links, array('class' => 'links inline'));
  
  $prev_next_links_content_type = (theme_get_setting('enable_content_type') == 1) ? $vars['node']->type : 'default';
  $prev_next_links_setting = (theme_get_setting('prev_next_links_'. $prev_next_links_content_type) == 1);
  
  if ($prev_next_links_setting) {
    // generate previous-next links
    $prev_next_links = _plainscape_prev_next_links($vars['node']);
    
    // only add the links when it is not teaser and not in preview mode
    if (!$teaser && $vars['node']->op != 'Preview' && !empty($prev_next_links)) {
      $vars['prev_next_links'] = $prev_next_links;
    }
  }
}

/**
 * Returns a formatted list of recent comments to be displayed in the comment block.
 *
 * Override the default number of 10 recent comments
 * and display the configured number of recent comments (default to 5).
 */
function phptemplate_comment_block() {
  $items = array();
  foreach (comment_get_recent(theme_get_setting('recent_comments')) as $comment) {
    $items[] = l($comment->subject, 'node/'. $comment->nid, array('fragment' => 'comment-'. $comment->cid)) .'<br />'. t('@time ago', array('@time' => format_interval(time() - $comment->timestamp)));
  }
  if ($items) {
    return theme('item_list', $items);
  }
}

/**
 * Displays previous/next links on each node
 */
function _plainscape_prev_next_links($node) {
  $output = '';
  $max_length = 28;

  $sql = "SELECT nid, title FROM {node} n WHERE n.type = '%s' AND n.status = %d AND n.created < %d ORDER BY n.created DESC";
  $result = db_query_range(db_rewrite_sql($sql), $node->type, 1, $node->created, 0, 1);
  $prev = db_fetch_object($result);
  
  $title_trimmed = $prev->title;
  
  if (drupal_strlen($prev->title) > $max_length) {
    $title_trimmed = drupal_substr($prev->title, 0, $max_length - 4) . '...';
  }
  
  if ($prev) {
    $output .= '<div class="previous-link">';
    $output .= l('<strong>&laquo; ' . t('Previous') . ' | </strong>' . $title_trimmed,
      'node/' . $prev->nid,
      array(
        'attributes' => array('title' => t('Read "@previous-node-title"', array('@previous-node-title' => $prev->title))),
        'html' => true,
      )
    );
    $output .= '</div>';
  }
  
  $sql = "SELECT nid, title FROM {node} n WHERE n.type = '%s' AND n.status = %d AND n.created > %d ORDER BY n.created ASC";
  $result = db_query_range(db_rewrite_sql($sql), $node->type, 1, $node->created, 0, 1);
  $next = db_fetch_object($result);
  
  $title_trimmed = $next->title;
  
  if (drupal_strlen($next->title) > $max_length) {
    $title_trimmed = drupal_substr($next->title, 0, $max_length - 4) . '...';
  }
  
  if ($next) {
    $output .= '<div class="next-link">';
    $output .= l($title_trimmed . '<strong> | ' . t('Next') . ' &raquo;</strong>',
      'node/' . $next->nid,
      array(
        'attributes' => array('title' => t('Read "@next-node-title"', array('@next-node-title' => $next->title))),
        'html' => true,
      )
    );
    $output .= '</div>';
  }
  
  return $output;
}

/**
 * Recursively remove [#children] and [#printed] array entries
 * from content array to make it ready for drupal_render()
 */
function plainscape_prepare_content(&$content) {
  if (isset($content['#children'])) {
    unset($content['#children']);
  }
  
  if (isset($content['#printed'])) {
    unset($content['#printed']);
  }
    
  foreach ($content as $key => $value) {
    if (is_array($value)) {
      plainscape_prepare_content($content[$key]);
    }
  }
}
